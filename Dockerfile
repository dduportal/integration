FROM java:8 

# Install maven
RUN apt-get update
RUN apt-get install -y maven

WORKDIR /projet

# Prepare by downloading dependencies
ADD pom.xml /projet/pom.xml
RUN ["mvn", "dependency:resolve"]
RUN ["mvn", "verify"]

# Adding source, compile and package into a fat jar
ADD src /projet/src
ADD *.xml /projet/
RUN ["mvn", "package"]

EXPOSE 4567
CMD ["/usr/lib/jvm/java-8-openjdk-amd64/bin/java", "-jar", "target/projetPerso-1.0-SNAPSHOT.jar"]